#!/bin/sh

echo "Checking private key"
: "${SSH_PRIVATE_KEY:?|***| No private key set. Please set the \$SSH_PRIVATE_KEY variable |***|}"

echo "Checking user"
: "${SSH_USER:?|***| No user set. Please set the \$SSH_USER variable |***|}"

echo "Checking host"
: "${SSH_HOST:?|***| No host set. Please set the \$SSH_HOST variable |***|}"

echo "Starting SSH Agent"
eval $(ssh-agent -s)

echo "Adding private key to agent store"
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -> /dev/null

echo "Verifying the SSH host keys"
mkdir -p ~/.ssh
chmod 700 ~/.ssh
ssh-keyscan $SSH_HOST >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

if [[ -n "${COPY_SRC}" ]]; then
  echo "Starting copy"
  ssh $SSH_USER@$SSH_HOST "mkdir ssh-commander"
  scp ${COPY_SRC} $SSH_USER@$SSH_HOST:.
fi

echo "Starting SSH command executor"
echo "Executing $@ on $SSH_USER@$SSH_HOST"
ssh $SSH_USER@$SSH_HOST $@

if [[ -n "${COPY_SRC}" ]]; then
  echo "Deleting copied file"
  ssh $SSH_USER@$SSH_HOST "rm -r ssh-commander"
  scp ${COPY_SRC} $SSH_USER@$SSH_HOST:.
fi

