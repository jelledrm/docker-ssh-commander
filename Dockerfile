FROM alpine:3.7

RUN apk add --no-cache \
    openssh

COPY entrypoint.sh .

ENTRYPOINT ["sh","/entrypoint.sh"]